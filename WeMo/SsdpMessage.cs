﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HomeOS.Hub.Scouts.WeMo
{
    class SsdpMessage
    {
        #region Properties
        public bool IsValid { get; set; }

        public IPAddress SourceIp { get; set; }

        public string this[string key]
        {
            get { return _fields[key]; }
            set { _fields[key] = value; }
        }

        #endregion Properties

        #region Fields
        protected readonly Dictionary<string, string> _fields; 
        #endregion Fields

        #region Public Methods
        public SsdpMessage(string msg, IPEndPoint ep)
        {
            IsValid = true;
            SourceIp = ep.Address;
            _fields = new Dictionary<string, string>();

            try
            {
                string[] lines = msg.Split('\n');
                Regex splitByColon = new Regex(@"^([^:]+)(:)(.*)$"); // Match to first colon, then to end
                foreach (Match match in lines.Select(s => splitByColon.Match(s)))
                {
                    if (!match.Success) continue;

                    string header = match.Groups[1].Value, value = match.Groups[3].Value;
                    _fields.Add(header.ToLower(), value.ToLower());
                }
            }
            catch (Exception)
            {
                IsValid = false;
                _fields.Clear();
            }
        }
        #endregion Public Methods
    }
}
