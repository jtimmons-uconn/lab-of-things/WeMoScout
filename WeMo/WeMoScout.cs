﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using HomeOS.Hub.Common;
using HomeOS.Hub.Platform.DeviceScout;
using HomeOS.Hub.Platform.Views;

namespace HomeOS.Hub.Scouts.WeMo
{
    public class WeMoScout : IScout
    {
        #region Fields
        #region CONSTANTS
        private readonly TimeSpan _deviceSearchFreq = new TimeSpan(0, 0, 1, 0); // One Minute
        private readonly TimeSpan _timeout = new TimeSpan(0, 0, 0, 10);

        private const string _driverName = "WeMoDriver";
        private const int _listenPort = 19000;
        private const string _multicastIp = "239.255.255.250";
        private const string _multicastPort = "1900";
        private const string _multicastMsg = "M-SEARCH * HTTP/1.1\r\n" +
              "HOST:239.255.255.250:1900\r\n" +
              "ST:upnp:rootdevice\r\n" +
              "MAN:\"ssdp:discover\"\r\n" +
              "MX:3\r\n\r\n";
        #endregion CONSTANTS

        private UdpClient _client;
        private VLogger _logger;
        private DeviceList _devices;
        #endregion Fields

        private WeMoScoutService scoutService;
        private WebFileServer appServer;

        #region Public Methods
        public void Init(string baseUrl, string baseDir, ScoutViewOfPlatform platform, VLogger logger)
        {
            // Initialize Values
            _logger = logger;
            _devices = new DeviceList();

            // Start some shit
            scoutService = new WeMoScoutService(baseUrl + "/webapp", this, platform, logger);
            appServer = new WebFileServer(baseDir, baseUrl, logger);

            // Create UDP Connection for Devices
            IPEndPoint localEp = new IPEndPoint(IPAddress.Any, _listenPort);
            _client = new UdpClient(localEp);
            _client.MulticastLoopback = _client.EnableBroadcast = true;

            // Populate list from time to time
            PopulateList();
        }

        public void PopulateList()
        {
            // Make a new thread that populates the list and returns it after a while
            SafeThread thread = new SafeThread(FillDeviceList, "WeMoScoutDeviceFinder", _logger);
            thread.Start();

            Timer t = new Timer((o) => thread.Abort(), null, _timeout, Timeout.InfiniteTimeSpan); // Timeout
        }

        public List<Device> GetDevices()
        {
            PopulateList();
            return _devices.GetClonedList();
        }

        public string GetInstructions()
        {
            return "Placeholder";
        }

        public void Dispose()
        {
            _client.Close();
        }
        #endregion Public Methods

        #region Private Methods
        private void FillDeviceList()
        {
            // Create an endpoint to send the message to
            IPEndPoint multicastEp = new IPEndPoint(IPAddress.Parse(_multicastIp), int.Parse(_multicastPort));
            IPEndPoint recEp = new IPEndPoint(IPAddress.Any, 0);

            // Send the message
            byte[] bytes = Encoding.UTF8.GetBytes(_multicastMsg);
            _client.Send(bytes, bytes.Length, multicastEp);

            while (true)
            {
                byte[] msg = _client.Receive(ref recEp);
                ProcessMessage(msg, recEp);
            }
        }

        private void ProcessMessage(byte[] bytes, IPEndPoint remoteHost)
        {
            WeMoSsdpMessage msg = new WeMoSsdpMessage(Encoding.UTF8.GetString(bytes), remoteHost);
            
            if (msg.IsValid)
                AddDevice(msg.DeviceName, msg.DeviceUniqueName, msg.SourceIp.ToString(), msg.TimeStamp);
            
        }

        private void AddDevice(string name, string uniqueName, string ip, DateTime timestamp)
        {
            //Device d = new Device(name, uniqueName, ip, timestamp, _driverName, false);
            Device d = new Device(name, uniqueName, ip, timestamp, "HomeOS.Hub.Drivers.WeMo", false);
            d.Details.DriverParams = new List<string>() { d.UniqueName, d.DeviceIpAddress };
            _devices.InsertDevice(d);
        }
        #endregion Private Methods
    }
}
