﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HomeOS.Hub.Scouts.WeMo
{
    class InValidWeMoSsdpMessageException : Exception { }

    class WeMoSsdpMessage : SsdpMessage
    {
        #region Properties
        public string DeviceName
        {
            get { return _deviceName ?? (_deviceName = GetDeviceName()); }
        }

        public string DeviceUniqueName
        {
            get { return _deviceUniqueName ?? (_deviceUniqueName = GetDeviceUniqueName()); }
        }

        public DateTime TimeStamp
        {
            get { return _timeStamp ?? (_timeStamp = GetTimeStamp()).Value; }
        }
        #endregion Properties

        #region Fields
        private string _deviceName;
        private string _deviceUniqueName;
        private DateTime? _timeStamp;
        #endregion Fields

        #region Public Methods
        public WeMoSsdpMessage(string msg, IPEndPoint ep) : base(msg, ep)
        {
            string value;
            IsValid = _fields.TryGetValue("x-user-agent", out value) && value.Contains("redsonic");
        }
        #endregion Public Methods

        #region Private Methods
        private string GetDeviceName()
        {
            string rawNameField;
            
            if (!_fields.TryGetValue("usn", out rawNameField))
                throw new InValidWeMoSsdpMessageException();

            Match match = Regex.Match(rawNameField, @"uuid:([a-z]+)"); // () Creates a group
            return (match.Success) ? match.Groups[1].Value : rawNameField;
        }

        private string GetDeviceUniqueName()
        {
            string rawUniqueNameField;

            if (!_fields.TryGetValue("usn", out rawUniqueNameField))
                throw new InValidWeMoSsdpMessageException();

            Match match = Regex.Match(rawUniqueNameField, @"uuid:(.*)::upnp"); // () Creates a group
            return (match.Success) ? match.Groups[1].Value : rawUniqueNameField;
        }

        private DateTime GetTimeStamp()
        {
            string dateString;

            if (!_fields.TryGetValue("date", out dateString))
                throw new InValidWeMoSsdpMessageException();

            return DateTime.Parse(dateString);
        }
        #endregion Private Methods
    }
}
